﻿using DiDrDe.MessageBus.Messages;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus
{
    public interface IQueryConsumer<in TQuery, TQueryResponse>
        where TQuery : class, IQuery
        where TQueryResponse : class, IMessage
    {
        Task<TQueryResponse> Consume(TQuery query);
    }
}