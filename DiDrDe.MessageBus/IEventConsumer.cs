﻿using DiDrDe.MessageBus.Messages;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus
{
    public interface IEventConsumer<in TEvent>
        where TEvent : IEvent
    {
        Task Consume(TEvent @event);
    }
}