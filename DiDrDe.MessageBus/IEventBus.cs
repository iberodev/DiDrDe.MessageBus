﻿using DiDrDe.MessageBus.Messages;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus
{
    public interface IEventBus
    {
        Task Publish<TEvent>(TEvent @event)
            where TEvent : class, IEvent;
    }
}