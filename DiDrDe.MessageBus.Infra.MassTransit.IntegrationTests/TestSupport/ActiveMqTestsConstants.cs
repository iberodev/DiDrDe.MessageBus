﻿namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport
{
    public class ActiveMqTestsConstants
    {
        public static string HostName = "127.0.0.1"; //"webcenter__activemq";
        public static string Username = "admin";
        public static string Password = "admin";
        public static int Port = 61616;
        public static bool UseSsl = false;
        public static bool AutoDelete = true;
    }
}