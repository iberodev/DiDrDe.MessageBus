﻿using System.Threading.Tasks;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Consumers
{
    public class FakeCommandConsumer
        : ICommandConsumer<IFakeCommand, IFakeResponse>
    {
        public Task<IFakeResponse> Consume(IFakeCommand command)
        {
            var id = command.Id;
            var response = new FakeResponse(id);
            return Task.FromResult<IFakeResponse>(response);
        }
    }
}