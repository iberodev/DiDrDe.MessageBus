﻿using System;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport
{
    public class FakeEvent
        : IFakeEvent
    {
        public Guid Id { get; }

        public FakeEvent(Guid id)
        {
            Id = id;
        }
    }
}