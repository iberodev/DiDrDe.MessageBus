﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using DiDrDe.MessageBus.Sample.Common;
using DiDrDe.MessageBus.Sample.Common.Messages;
using MassTransit.Util;
using System;
using System.Threading.Tasks;

#pragma warning disable 1998

namespace DiDrDe.MessageBus.Sample.CommandConsumer
{
    public class Program
    {
        static void Main()
        {
            Console.WriteLine("Command receiver sample");
            var containerBuilder = new ContainerBuilder();

            containerBuilder
                .RegisterType<CommandOneConsumer>()
                .As<ICommandConsumer<ICommandOne, ICommandOneResult>>();

            containerBuilder
                .RegisterActiveMqCommandConsumer(
                    commandOptions => { commandOptions.ConsumesCommand<ICommandOne, ICommandOneResult>(); },
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = Constants.ActiveMqConstants.HostName,
                                EndpointName = "test_queue_commands",
                                Port = Constants.ActiveMqConstants.Port,
                                Username = Constants.ActiveMqConstants.Username,
                                Password = Constants.ActiveMqConstants.Password,
                                UseSsl = Constants.ActiveMqConstants.UseSsl,
                                AutoDelete = Constants.ActiveMqConstants.AutoDelete
                            };
                        return messageBusOptions;
                    });
            var container = containerBuilder.Build();
            var componentContext = container.Resolve<IComponentContext>();
            var busManager = componentContext.Resolve<ICommandConsumerMessageBusManager>();
            TaskUtil.Await(() => busManager.Start());
            Console.WriteLine("Press any key to exit");
            Console.In.ReadLine();
            TaskUtil.Await(() => busManager.Stop());
        }
    }

    public class CommandOneConsumer
        : ICommandConsumer<ICommandOne, ICommandOneResult>
    {
        public async Task<ICommandOneResult> Consume(ICommandOne command)
        {
            var commandCreatedOn = command.CreatedOn;
            var commandText = command.Text;
            Console.WriteLine($"Received {command} on {commandCreatedOn} with text: {commandText}");
            var result = new CommandOneResult();
            return result;
        }
    }
}