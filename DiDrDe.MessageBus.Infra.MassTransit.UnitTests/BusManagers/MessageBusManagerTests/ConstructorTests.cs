﻿using DiDrDe.MessageBus.Infra.MassTransit.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using FluentAssertions;
using Moq;
using System;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.BusManagers.MessageBusManagerTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private MessageBusManager<IMessageBusControl> _sut;
            private IMessageBusControl _messageBusControl;

            protected override void Given()
            {
                _messageBusControl = Mock.Of<IMessageBusControl>();
            }

            protected override void When()
            {
                _sut = new MessageBusManager<IMessageBusControl>(_messageBusControl);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_ICommandConsumerMessageBusManager()
            {
                _sut.Should().BeAssignableTo<ICommandConsumerMessageBusManager>();
            }

            [Fact]
            public void Then_It_Should_Be_A_ICommandSenderMessageBusManager()
            {
                _sut.Should().BeAssignableTo<ICommandSenderMessageBusManager>();
            }

            [Fact]
            public void Then_It_Should_Be_A_IEventConsumerMessageBusManager()
            {
                _sut.Should().BeAssignableTo<IEventConsumerMessageBusManager>();
            }

            [Fact]
            public void Then_It_Should_Be_An_IDisposable()
            {
                _sut.Should().BeAssignableTo<IDisposable>();
            }
        }
    }
}