﻿using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using FluentAssertions;
using Moq;
using System;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.CommandBusTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private CommandBus _sut;
            private ICommandSenderMessageBusControl _busControl;

            protected override void Given()
            {
                _busControl = Mock.Of<ICommandSenderMessageBusControl>();
            }

            protected override void When()
            {
                _sut = new CommandBus(_busControl);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_ICommandBus()
            {
                _sut.Should().BeAssignableTo<ICommandBus>();
            }
        }

        public class Given_Invalid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private CommandBus _sut;
            private ICommandSenderMessageBusControl _busControl;
            private ArgumentNullException _argumentNullException;

            protected override void Given()
            {
                _busControl = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = new CommandBus(_busControl);
                }
                catch (ArgumentNullException exception)
                {
                    _argumentNullException = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Have_Thrown_An_ArgumentNullException()
            {
                _argumentNullException.Should().NotBeNull();
            }
        }
    }
}