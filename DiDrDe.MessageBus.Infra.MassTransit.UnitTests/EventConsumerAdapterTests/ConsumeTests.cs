﻿using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts;
using MassTransit;
using Moq;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.EventConsumerAdapterTests
{
    public static class ConsumeTests
    {
        public class Given_An_EventDtoConsumerAdapter_When_Consuming
            : Given_WhenAsync_Then_Test
        {
            private EventConsumerAdapter<IFakeEvent> _sut;
            private ConsumeContext<IFakeEvent> _consumeContext;
            private Mock<IEventConsumer<IFakeEvent>> _eventConsumerMock;

            protected override void Given()
            {
                _consumeContext = Mock.Of<ConsumeContext<IFakeEvent>>();
                _eventConsumerMock = new Mock<IEventConsumer<IFakeEvent>>();
                var eventConsumer = _eventConsumerMock.Object;
                _sut = new EventConsumerAdapter<IFakeEvent>(eventConsumer);
            }

            protected override async Task WhenAsync()
            {
                await _sut.Consume(_consumeContext);
            }

            [Fact]
            public void Then_It_Should_Use_The_EventConsumer_To_Consume_The_Event()
            {
                _eventConsumerMock.Verify(x => x.Consume(It.IsAny<IFakeEvent>()));
            }
        }
    }
}