﻿using DiDrDe.MessageBus.Messages;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts
{
    public interface IFakeEvent
        : IEvent
    {
    }
}