﻿using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport
{
    public class FakeCommand
        : IFakeCommand
    {
    }
}