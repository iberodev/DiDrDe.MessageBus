﻿using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.Configuration.ActiveMqOptionsTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private ActiveMqOptions _sut;

            protected override void Given()
            {
            }

            protected override void When()
            {
                _sut = new ActiveMqOptions();
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }
        }
    }
}