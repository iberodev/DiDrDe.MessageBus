﻿using System;
using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport;
using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts;
using FluentAssertions;
using MassTransit;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.CommandConsumerAdapterTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
           : Given_When_Then_Test
        {
            private CommandConsumerAdapter<IFakeCommand, IFakeResponse> _sut;
            private ICommandConsumer<IFakeCommand, IFakeResponse> _commandConsumer;

            protected override void Given()
            {
                _commandConsumer = Mock.Of<ICommandConsumer<IFakeCommand, IFakeResponse>>();
            }

            protected override void When()
            {
                _sut = new CommandConsumerAdapter<IFakeCommand, IFakeResponse>(_commandConsumer);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IConsumer_Of_The_Command()
            {
                _sut.Should().BeAssignableTo<IConsumer<FakeCommand>>();
            }
        }

        public class Given_Invalid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private CommandConsumerAdapter<IFakeCommand, IFakeResponse> _sut;
            private ICommandConsumer<IFakeCommand, IFakeResponse> _commandConsumer;
            private ArgumentNullException _argumentNullException;

            protected override void Given()
            {
                _commandConsumer = null;
            }

            protected override void When()
            {
                try
                {
                    _sut = new CommandConsumerAdapter<IFakeCommand, IFakeResponse>(_commandConsumer);
                }
                catch (ArgumentNullException exception)
                {
                    _argumentNullException = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Be_Null()
            {
                _sut.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Have_Thrown_An_ArgumentNullException()
            {
                _argumentNullException.Should().NotBeNull();
            }
        }
    }
}