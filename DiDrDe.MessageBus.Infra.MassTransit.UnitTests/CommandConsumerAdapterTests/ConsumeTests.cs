﻿using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts;
using MassTransit;
using Moq;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.CommandConsumerAdapterTests
{
    public static class ConsumeTests
    {
        public class Given_An_CommandConsumerAdapter_When_Consuming
            : Given_WhenAsync_Then_Test
        {
            private CommandConsumerAdapter<IFakeCommand, IFakeResponse> _sut;
            private ConsumeContext<IFakeCommand> _consumeContext;
            private Mock<ICommandConsumer<IFakeCommand, IFakeResponse>> _commandConsumerMock;

            protected override void Given()
            {
                _consumeContext = Mock.Of<ConsumeContext<IFakeCommand>>();
                _commandConsumerMock = new Mock<ICommandConsumer<IFakeCommand, IFakeResponse>>();
                var commandConsumer = _commandConsumerMock.Object;
                _sut = new CommandConsumerAdapter<IFakeCommand, IFakeResponse>(commandConsumer);
            }

            protected override async Task WhenAsync()
            {
                await _sut.Consume(_consumeContext);
            }

            [Fact]
            public void Then_It_Should_Use_The_CommandConsumer_To_Consume_The_Command()
            {
                _commandConsumerMock.Verify(x => x.Consume(It.IsAny<IFakeCommand>()));
            }
        }
    }
}