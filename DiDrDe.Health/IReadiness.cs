﻿using System.Threading.Tasks;

namespace DiDrDe.Health
{
    public interface IReadiness
    {
        Task<bool> IsReady();
    }
}
