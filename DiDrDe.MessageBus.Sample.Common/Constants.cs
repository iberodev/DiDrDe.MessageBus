﻿namespace DiDrDe.MessageBus.Sample.Common
{
    public static class Constants
    {
        public static class ActiveMqConstants
        {
            public const string HostName = "localhost";
            public const int Port = 61616;
            public const string Username = "admin";
            public const string Password = "admin";
            public const bool UseSsl = false;
            public const bool AutoDelete = true;
        }
    }
}