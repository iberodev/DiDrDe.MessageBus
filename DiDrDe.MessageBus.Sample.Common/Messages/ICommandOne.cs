﻿using System;
using DiDrDe.MessageBus.Messages;

namespace DiDrDe.MessageBus.Sample.Common.Messages
{
    public interface ICommandOne
        : ICommand
    {
        string Text { get; }
        DateTime CreatedOn { get; }
    }
}