﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Sample.Common;
using DiDrDe.MessageBus.Sample.Common.Messages;
using MassTransit.Util;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus.Sample.EventPublisher
{
    public class Program
    {
        static void Main()
        {
            Console.WriteLine("Event publisher sample");
            var containerBuilder = new ContainerBuilder();
            containerBuilder
                .RegisterActiveMqEventPublisher(
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = Constants.ActiveMqConstants.HostName,
                                Port = Constants.ActiveMqConstants.Port,
                                Username = Constants.ActiveMqConstants.Username,
                                Password = Constants.ActiveMqConstants.Password,
                                UseSsl = Constants.ActiveMqConstants.UseSsl,
                                AutoDelete = Constants.ActiveMqConstants.AutoDelete
                            };
                        return messageBusOptions;
                    });

            var container = containerBuilder.Build();
            var componentContext = container.Resolve<IComponentContext>();
            TaskUtil.Await(() => PublishEvents(componentContext, 10, 200));
            Console.WriteLine("Press any key to exit");
            Console.In.ReadLine();
        }

        private static async Task PublishEvents(IComponentContext componentContext, int numberCommands, int gapMilliSeconds)
        {
            var eventBus = componentContext.Resolve<IEventBus>();
            for (int index = 0; index < numberCommands; index++)
            {
                Thread.Sleep(gapMilliSeconds);
                var id = Guid.NewGuid();
                var eventOne = new EventOne(id);
                Console.WriteLine($"Sending event {eventOne}");
                await eventBus.Publish<IEventOne>(eventOne);
            }
        }
    }
}