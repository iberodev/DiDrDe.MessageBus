﻿using DiDrDe.MessageBus.Sample.Common.Messages;
using System;

namespace DiDrDe.MessageBus.Sample.EventPublisher
{
    public class EventOne
        : IEventOne
    {
        public Guid Id { get; }
        public DateTime CreatedOn { get; }

        public EventOne(Guid id)
        {
            Id = id;
            CreatedOn = DateTime.UtcNow;
        }

        public override string ToString()
        {
            return $"Event with id {Id} created on {CreatedOn}";
        }
    }
}