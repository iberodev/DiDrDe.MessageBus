﻿using Autofac;
using DiDrDe.Health;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.Factories;
using DiDrDe.MessageBus.Infra.MassTransit.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using MassTransit;
using System;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac
{
    public static class ReadinessExtensions
    {
        public static ContainerBuilder RegisterActiveMqReadinessChecker(
            this ContainerBuilder builder,
            Func<IComponentContext, ActiveMqOptions> optionsRetriever)
        {
            builder
                .Register(context =>
                {
                    var busControlWrapper =
                        ActiveMqBusControlWrapperFactory.CreateForHealthChecker(context, optionsRetriever);
                    return busControlWrapper;
                })
                .As<IReadinessMessageBusControl>()
                .As<IBus>()
                .SingleInstance();

            builder
                .RegisterType<MessageBusManager<IReadinessMessageBusControl>>()
                .As<IReadinessMessageBusManager>()
                .As<IReadiness>()
                .SingleInstance();

            return builder;
        }
    }
}