﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.Factories;
using DiDrDe.MessageBus.Infra.MassTransit.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using System;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac
{
    public static class CommandConsumerExtensions
    {
        public static ContainerBuilder RegisterActiveMqCommandConsumer(
            this ContainerBuilder builder,
            Action<CommandTypesOptions> commandOptions,
            Func<IComponentContext, ActiveMqOptions> optionsRetriever)
        {
            var integrationCommandOptions = new CommandTypesOptions();
            commandOptions?.Invoke(integrationCommandOptions);
            var commandsTypes = integrationCommandOptions.CommandTypes;

            foreach (var types in commandsTypes)
            {
                var typeArguments =
                    new[]
                    {
                        types.Key,
                        types.Value
                    };

                var adapterTypeWithTypeArguments = typeof(CommandConsumerAdapter<,>).MakeGenericType(typeArguments);

                builder
                    .RegisterType(adapterTypeWithTypeArguments)
                    .SingleInstance();
            }

            builder
                .Register(context =>
                {
                    var busControlWrapper =
                        ActiveMqBusControlWrapperFactory.CreateForCommandConsumer(context, optionsRetriever, commandsTypes);
                    return busControlWrapper;
                })
                .As<ICommandConsumerMessageBusControl>()
                .SingleInstance();

            builder
                .RegisterType<MessageBusManager<ICommandConsumerMessageBusControl>>()
                .As<ICommandConsumerMessageBusManager>()
                .SingleInstance();

            return builder;
        }
    }
}