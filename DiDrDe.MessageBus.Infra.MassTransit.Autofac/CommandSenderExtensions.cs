﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.Factories;
using DiDrDe.MessageBus.Infra.MassTransit.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using System;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac
{
    public static class CommandSenderExtensions
    {
        public static ContainerBuilder RegisterActiveMqCommandSender(
            this ContainerBuilder builder,
            Action<CommandTypesOptions> commandOptions,
            Func<IComponentContext, ActiveMqOptions> optionsRetriever)
        {
            var commandsOptions = new CommandTypesOptions();
            commandOptions?.Invoke(commandsOptions);
            var commandTypes = commandsOptions.CommandTypes;

            builder
                .Register(context =>
                {
                    var busControlWrapper =
                        ActiveMqBusControlWrapperFactory.CreateForCommandSender(context, optionsRetriever, commandTypes);
                    return busControlWrapper;
                })
                .As<ICommandSenderMessageBusControl>()
                .SingleInstance();

            builder
                .RegisterType<MessageBusManager<ICommandSenderMessageBusControl>>()
                .As<ICommandSenderMessageBusManager>()
                .SingleInstance();

            builder
                .RegisterType<CommandBus>()
                .As<ICommandBus>()
                .SingleInstance();

            return builder;
        }
    }
}