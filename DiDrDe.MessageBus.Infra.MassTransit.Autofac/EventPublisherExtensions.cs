﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.Factories;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using MassTransit;
using System;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac
{
    public static class EventPublisherExtensions
    {
        public static ContainerBuilder RegisterActiveMqEventPublisher(
            this ContainerBuilder builder,
            Func<IComponentContext, ActiveMqOptions> optionsRetriever)
        {
            builder
                .Register(context =>
                {
                    var busControlWrapper =
                        ActiveMqBusControlWrapperFactory.CreateForEventPublisher(context, optionsRetriever);
                    return busControlWrapper;
                })
                .As<IPublishEndpoint>()
                .SingleInstance();

            builder
                .RegisterType<EventBus>()
                .As<IEventBus>()
                .SingleInstance();

            return builder;
        }
    }
}