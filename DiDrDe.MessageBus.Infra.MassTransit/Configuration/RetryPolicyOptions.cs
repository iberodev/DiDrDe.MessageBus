﻿namespace DiDrDe.MessageBus.Infra.MassTransit.Configuration
{
    public class RetryPolicyOptions
    {
        public int RetryAttempts { get; set; }
        public int RetryIntervalMilliseconds { get; set; }
    }
}