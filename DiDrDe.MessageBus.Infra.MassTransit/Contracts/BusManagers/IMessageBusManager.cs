﻿using System.Threading.Tasks;

namespace DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers
{
    public interface IMessageBusManager
    {
        Task Start(int maxMillisecondsWait = 50000);
        Task Stop();
    }
}